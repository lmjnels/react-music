# React-Music

React front end client that communicates with the Spotify API

```
yarn
yarn start
visit http://localhost:3000
```

Please note that the Spotify API only provides 30 second audio snippets. It may be possible to get the full track by using the [Web Playback SDK](https://beta.developer.spotify.com/documentation/web-playback-sdk/).

